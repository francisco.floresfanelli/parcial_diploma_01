﻿using System.Collections.Generic;
using System.Data;

namespace Data
{
    internal interface IConexion
    {
        void Save(string query, Dictionary<string, object> data = null, CommandType type = CommandType.Text);
    }
}