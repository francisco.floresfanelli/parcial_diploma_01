﻿using Entities;
using System;
using System.Configuration;

namespace Data
{
    public class DisparoRepository
    {
        private IConexion _conexion;

        public DisparoRepository()
        {
            this._conexion = new Conexion();
        }

        public void Save(Disparos disparo)
        {
            var useDb = Convert.ToBoolean(ConfigurationManager.AppSettings["SaveDB"].ToString());

            var query = "Insert into Disparos(Arma, Distancia, Veces, Acierto) values(@Arma, @Distancia, @Veces, @Acierto)";

            var parametros = new System.Collections.Generic.Dictionary<string, object>
            {
                { "Arma", disparo.Arma },
                { "Distancia", disparo.Distancia },
                { "Veces", disparo.Veces },
                { "Acierto", disparo.Acierto }
            };

            if (useDb) this._conexion.Save(query, parametros);
        }
    }
}