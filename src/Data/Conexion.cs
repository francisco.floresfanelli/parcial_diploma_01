﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Data
{
    public class Conexion : IConexion
    {
        private string _conn;

        public Conexion()
        {
            _conn = ConfigurationManager.ConnectionStrings["data"].ConnectionString;
        }

        public void Save(string query, Dictionary<string, object> data = null, CommandType type = CommandType.Text)
        {
            using (var cn = new SqlConnection(_conn))
            {
                cn.Open();

                using (var cmd = new SqlCommand(query, cn) { CommandType = type })
                {
                    if (data != null && data.Any())
                    {
                        foreach (var d in data) { cmd.Parameters.AddWithValue($"@{d.Key}", d.Value); }
                    }

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}