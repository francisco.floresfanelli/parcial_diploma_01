﻿using Data;
using Entities;
using System.Collections.Generic;

namespace Core
{
    public class CentroInteligencia
    {
        private DisparoRepository _persistencia;
        private int _contadorDisparos;
        private List<Disparos> _disparosRealizados = new List<Disparos>();
        private float _distacionDisparo;
        private ArmaStategy _stategy;
        private bool objetivoSeleccionado;

        public CentroInteligencia()
        {
            this._persistencia = new DisparoRepository();
            this._contadorDisparos = 1;
        }

        public void SetDistancia(float distancia)
        {
            this._distacionDisparo = distancia;

            this.objetivoSeleccionado = true;
        }

        private void SetStrategy(ArmaStategy arma)
        {
            this._stategy = arma;
        }

        public bool ObjetivoSeleccionado()
        {
            return this.objetivoSeleccionado;
        }

        public bool Atacar(AvailableWeapons arma)
        {
            this.GetStrategy(arma);

            var ataque = _stategy.Atacar(_distacionDisparo);

            var disparo = new Disparos()
            {
                Veces = _contadorDisparos++,
                Acierto = ataque,
                Distancia = _distacionDisparo.ToString(),
                Arma = _stategy.ToString()
            };

            _disparosRealizados.Add(disparo);

            _persistencia.Save(disparo);

            if (ataque)
            {
                this.objetivoSeleccionado = false;
                this._contadorDisparos = 1;
            }

            return ataque;
        }

        private void GetStrategy(AvailableWeapons arma)
        {
            switch (arma)
            {
                case AvailableWeapons.CANIONCORTO:
                    this.SetStrategy(new CanionCortoAlcance());
                    break;

                case AvailableWeapons.CANIONULTRASONICO:
                    this.SetStrategy(new CanionUltrasonico());
                    break;

                case AvailableWeapons.RAYOLASER:
                    this.SetStrategy(new RayoLaserDestructorBionico());
                    break;

                default:
                    throw new System.Exception("No hay armar disponible para la estategia seleccionada");
            }
        }
    }
}