﻿using Core;
using Entities;
using System;
using System.Windows.Forms;

namespace Presentation
{
    public partial class Main : Form
    {
        private CentroInteligencia _centroIteligencia;

        public Main()
        {
            InitializeComponent();
            this._centroIteligencia = new CentroInteligencia();
            BuscarObjetivo();
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            BuscarObjetivo();
        }

        private void BuscarObjetivo()
        {
            Random rnd = new Random();
            var min = 1;
            var max = 200;
            double range = max - min;
            double sample = rnd.NextDouble();
            double scaled = (sample * range) + min;
            float distaciaObjetivo = (float)scaled;

            this._centroIteligencia.SetDistancia(distaciaObjetivo);
            lbl_distance.Text = $"Objetivo detectado a \n{ distaciaObjetivo:0.00} mts";
        }

        private void btn_shoot_Click(object sender, EventArgs e)
        {
            if (!this._centroIteligencia.ObjetivoSeleccionado())
            {
                MessageBox.Show("Objetivo aun vivo!");
                return;
            }

            MostrarResultado(this._centroIteligencia.Atacar(AvailableWeapons.CANIONCORTO));
        }

        private void btn_shoot_ultra_Click(object sender, EventArgs e)
        {
            if (!this._centroIteligencia.ObjetivoSeleccionado())
            {
                MessageBox.Show("Objetivo aun vivo!");
                return;
            }

            MostrarResultado(this._centroIteligencia.Atacar(AvailableWeapons.CANIONULTRASONICO));
        }

        private void btn_shoot_laser_Click(object sender, EventArgs e)
        {
            if (!this._centroIteligencia.ObjetivoSeleccionado())
            {
                MessageBox.Show("Objetivo aun vivo!");
                return;
            }

            MostrarResultado(this._centroIteligencia.Atacar(AvailableWeapons.RAYOLASER));
        }

        private void MostrarResultado(bool definicionAtaque)
        {
            if (definicionAtaque)
                BuscarObjetivo();

            var message = new ResultadoAtaque(definicionAtaque) { StartPosition = FormStartPosition.CenterParent };
            message.ShowDialog();
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}