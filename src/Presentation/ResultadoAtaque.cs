﻿using System;
using System.Windows.Forms;

namespace Presentation
{
    public partial class ResultadoAtaque : Form
    {
        private readonly bool ataque;

        public ResultadoAtaque(bool ataque)
        {
            InitializeComponent();
            lbl_alive.Visible = false;
            lbl_destroy.Visible = false;
            this.ataque = ataque;
            SelectImage();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectImage()
        {
            if (ataque)
            {
                this.pictureBox1.Image = Properties.Resources.bombing_explosion;
                this.lbl_destroy.Visible = true;
            }
            else
            {
                this.pictureBox1.Image = Properties.Resources.objetivoVivo;
                this.lbl_alive.Visible = true;
            }
        }
    }
}