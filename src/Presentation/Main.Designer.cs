﻿
namespace Presentation
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btn_shoot = new System.Windows.Forms.Button();
            this.btn_shoot_ultra = new System.Windows.Forms.Button();
            this.btn_shoot_laser = new System.Windows.Forms.Button();
            this.lbl_distance = new System.Windows.Forms.Label();
            this.lbl_distance_label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_search = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.p_top = new System.Windows.Forms.Panel();
            this.p_bottom = new System.Windows.Forms.Panel();
            this.p_left = new System.Windows.Forms.Panel();
            this.p_right = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_shoot
            // 
            this.btn_shoot.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_shoot.FlatAppearance.BorderSize = 0;
            this.btn_shoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_shoot.Font = new System.Drawing.Font("Stencil", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_shoot.ForeColor = System.Drawing.Color.White;
            this.btn_shoot.Location = new System.Drawing.Point(16, 299);
            this.btn_shoot.Name = "btn_shoot";
            this.btn_shoot.Size = new System.Drawing.Size(177, 112);
            this.btn_shoot.TabIndex = 1;
            this.btn_shoot.Text = "Disparar\r\nCAÑON\r\nCORTO";
            this.btn_shoot.UseVisualStyleBackColor = false;
            this.btn_shoot.Click += new System.EventHandler(this.btn_shoot_Click);
            // 
            // btn_shoot_ultra
            // 
            this.btn_shoot_ultra.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_shoot_ultra.FlatAppearance.BorderSize = 0;
            this.btn_shoot_ultra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_shoot_ultra.Font = new System.Drawing.Font("Stencil", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_shoot_ultra.ForeColor = System.Drawing.Color.White;
            this.btn_shoot_ultra.Location = new System.Drawing.Point(217, 299);
            this.btn_shoot_ultra.Name = "btn_shoot_ultra";
            this.btn_shoot_ultra.Size = new System.Drawing.Size(177, 112);
            this.btn_shoot_ultra.TabIndex = 2;
            this.btn_shoot_ultra.Text = "Disparar\r\nCAÑON\r\nULTRASÓNICO";
            this.btn_shoot_ultra.UseVisualStyleBackColor = false;
            this.btn_shoot_ultra.Click += new System.EventHandler(this.btn_shoot_ultra_Click);
            // 
            // btn_shoot_laser
            // 
            this.btn_shoot_laser.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_shoot_laser.FlatAppearance.BorderSize = 0;
            this.btn_shoot_laser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_shoot_laser.Font = new System.Drawing.Font("Stencil", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_shoot_laser.ForeColor = System.Drawing.Color.White;
            this.btn_shoot_laser.Location = new System.Drawing.Point(418, 299);
            this.btn_shoot_laser.Name = "btn_shoot_laser";
            this.btn_shoot_laser.Size = new System.Drawing.Size(177, 112);
            this.btn_shoot_laser.TabIndex = 3;
            this.btn_shoot_laser.Text = "Disparar\r\nRAYO \r\nLASER";
            this.btn_shoot_laser.UseVisualStyleBackColor = false;
            this.btn_shoot_laser.Click += new System.EventHandler(this.btn_shoot_laser_Click);
            // 
            // lbl_distance
            // 
            this.lbl_distance.AutoSize = true;
            this.lbl_distance.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_distance.ForeColor = System.Drawing.Color.Red;
            this.lbl_distance.Location = new System.Drawing.Point(14, 18);
            this.lbl_distance.Name = "lbl_distance";
            this.lbl_distance.Size = new System.Drawing.Size(180, 27);
            this.lbl_distance.TabIndex = 4;
            this.lbl_distance.Text = "lbl_distance";
            // 
            // lbl_distance_label
            // 
            this.lbl_distance_label.AutoSize = true;
            this.lbl_distance_label.Font = new System.Drawing.Font("Stencil", 20F);
            this.lbl_distance_label.ForeColor = System.Drawing.Color.White;
            this.lbl_distance_label.Location = new System.Drawing.Point(31, 88);
            this.lbl_distance_label.Name = "lbl_distance_label";
            this.lbl_distance_label.Size = new System.Drawing.Size(286, 32);
            this.lbl_distance_label.TabIndex = 5;
            this.lbl_distance_label.Text = "Distancia Objetivo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Stencil", 18.25F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(170, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(258, 30);
            this.label3.TabIndex = 8;
            this.label3.Text = "ARMAS DISPONIBLES";
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_search.FlatAppearance.BorderSize = 0;
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_search.ForeColor = System.Drawing.Color.White;
            this.btn_search.Image = ((System.Drawing.Image)(resources.GetObject("btn_search.Image")));
            this.btn_search.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_search.Location = new System.Drawing.Point(366, 126);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(229, 101);
            this.btn_search.TabIndex = 0;
            this.btn_search.Text = "BUSCAR OTRO\r\nOBJETIVO";
            this.btn_search.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Stencil", 40F);
            this.label1.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.label1.Location = new System.Drawing.Point(141, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 64);
            this.label1.TabIndex = 9;
            this.label1.Text = "SARAZA S.A.";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(538, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 41);
            this.button1.TabIndex = 10;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.close_Click);
            // 
            // p_top
            // 
            this.p_top.BackColor = System.Drawing.Color.DarkRed;
            this.p_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.p_top.Location = new System.Drawing.Point(0, 0);
            this.p_top.Name = "p_top";
            this.p_top.Size = new System.Drawing.Size(617, 1);
            this.p_top.TabIndex = 12;
            // 
            // p_bottom
            // 
            this.p_bottom.BackColor = System.Drawing.Color.DarkRed;
            this.p_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.p_bottom.Location = new System.Drawing.Point(0, 474);
            this.p_bottom.Name = "p_bottom";
            this.p_bottom.Size = new System.Drawing.Size(617, 1);
            this.p_bottom.TabIndex = 13;
            // 
            // p_left
            // 
            this.p_left.BackColor = System.Drawing.Color.DarkRed;
            this.p_left.Dock = System.Windows.Forms.DockStyle.Right;
            this.p_left.Location = new System.Drawing.Point(616, 1);
            this.p_left.Name = "p_left";
            this.p_left.Size = new System.Drawing.Size(1, 473);
            this.p_left.TabIndex = 14;
            // 
            // p_right
            // 
            this.p_right.BackColor = System.Drawing.Color.DarkRed;
            this.p_right.Dock = System.Windows.Forms.DockStyle.Left;
            this.p_right.Location = new System.Drawing.Point(0, 1);
            this.p_right.Name = "p_right";
            this.p_right.Size = new System.Drawing.Size(1, 473);
            this.p_right.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(327, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(268, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Alumno: Francisco Andrés Flores Fanelli";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.lbl_distance);
            this.panel1.Location = new System.Drawing.Point(16, 127);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 100);
            this.panel1.TabIndex = 17;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(617, 475);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.p_right);
            this.Controls.Add(this.p_left);
            this.Controls.Add(this.p_bottom);
            this.Controls.Add(this.p_top);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_distance_label);
            this.Controls.Add(this.btn_shoot_laser);
            this.Controls.Add(this.btn_shoot_ultra);
            this.Controls.Add(this.btn_shoot);
            this.Controls.Add(this.btn_search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SARASA S.A.";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button btn_shoot;
        private System.Windows.Forms.Button btn_shoot_ultra;
        private System.Windows.Forms.Button btn_shoot_laser;
        private System.Windows.Forms.Label lbl_distance;
        private System.Windows.Forms.Label lbl_distance_label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel p_top;
        private System.Windows.Forms.Panel p_bottom;
        private System.Windows.Forms.Panel p_left;
        private System.Windows.Forms.Panel p_right;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}

