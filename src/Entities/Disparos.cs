﻿namespace Entities
{
    public class Disparos
    {
        public int? Id { get; set; }
        public string Arma { get; set; }
        public string Distancia { get; set; }
        public int Veces { get; set; }
        public bool Acierto { get; set; }
    }
}