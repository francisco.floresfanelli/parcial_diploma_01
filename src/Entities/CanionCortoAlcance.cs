﻿namespace Entities
{
    public class CanionCortoAlcance : ArmaStategy
    {
        public override bool Atacar(float distancia)
        {
            return distancia <= 10;
        }
    }
}