﻿namespace Entities
{
    public class RayoLaserDestructorBionico : ArmaStategy
    {
        public override bool Atacar(float distancia)
        {
            return distancia > 50 && distancia <= 200;
        }
    }
}