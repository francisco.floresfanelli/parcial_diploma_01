﻿namespace Entities
{
    public abstract class ArmaStategy
    {
        public abstract bool Atacar(float distancia);

        public override string ToString() {
            return this.GetType().Name;
        }
    }

    public enum AvailableWeapons
    {
        CANIONCORTO = 1, 
        CANIONULTRASONICO = 2,
        RAYOLASER = 3
    }
}